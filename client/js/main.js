const tableauLivres = document.querySelector('#tableauLivres')
const livresdb = [
    {
        nom:"L'algorthmique selon H2PROG",
        auteur:"Mathieu Gaston",
        pages:"200"
    },
    {
        nom:"Le monde des animaux",
        auteur:"Marc Merlin",
        pages:"500"
    },
    {
        nom:"La France du 19ème",
        auteur:"Albert Patrick",
        pages:"120"
    },
    {
        nom:"Le virus d'Asie",
        auteur:"Tya Mil",
        pages:"150"
    }
]


afficherLivres();

//Ajout et Affichage data dans le DOM
 function afficherLivres(){
   const listeLivres = document.querySelector("#tableauLivres tbody")
   let livres = "" ;
    for(let i = 0; i < livresdb.length; i++){

        livres += `<tr>
        <td>${livresdb[i].nom}</td>
        <td>${livresdb[i].auteur}</td>
        <td>${livresdb[i].pages}</td>
        <td><button class="btn btn-warning" onclick="afficherModifForm(${i})" >MODIFIER</button></td>
        <td><button class="btn btn-danger" onclick="supprimerLivre(${i})">SUPPRIMER</button></td> </tr>`
       
        }
        
        listeLivres.innerHTML = livres
 }
 
    
// Affichage Formulaire D'ajout livre

function AffichageFormulaire(){
document.querySelector("#formAjout").removeAttribute('class')
document.querySelector("#formModif").className = "d-none"
}




//Ajout Livre via formulaire
document.querySelector("#validationFormAjout").addEventListener('click', function(event){
event.preventDefault();
const titre =  document.querySelector("#formAjout #titre").value;
const auteur =  document.querySelector("#formAjout #auteur").value;
const pages =  document.querySelector("#formAjout #pages").value;
ajoutLivre(titre,auteur,pages)
document.querySelector("#formAjout").reset()
 document.querySelector("#formAjout").className = "d-none"
})

function ajoutLivre(titre,auteur,pages){
 const livre = {
     nom: titre,
     auteur: auteur,
     pages: pages
 }
 livresdb.push(livre)
 console.log(livresdb)
 afficherLivres()

}

//Supprimer Livre 
function supprimerLivre(position){
    if(confirm('Voulez-vous vraiment supprimer ?')){
        livresdb.splice(position,1)
        afficherLivres()
        alert('suppression effectuée')
    }
    else{
        alert('Suppression annulée !!')
    }

}

//Modification Livre

function afficherModifForm(position){
    document.querySelector("#formAjout").className = "d-none"
     document.querySelector("#formModif").removeAttribute('class')
     document.querySelector("#formModif #titre").value =  livresdb[position].nom;
    document.querySelector("#formModif #auteur").value =  livresdb[position].auteur;
     document.querySelector("#formModif #pages").value =  livresdb[position].pages;
     document.querySelector("#formModif #identifiant").value =  position;
}

document.querySelector("#validationFormModif").addEventListener('click',function(event){
event.preventDefault();
const titre =  document.querySelector("#formModif #titre").value;
const auteur =  document.querySelector("#formModif #auteur").value;
const pages =  document.querySelector("#formModif #pages").value;
const position =  document.querySelector("#formModif #identifiant").value 
livresdb[position].nom = titre;
livresdb[position].auteur = auteur;
livresdb[position].pages = pages;

afficherLivres();
 

})
